#include <iostream>
#include <sstream>
#include <vector>
#include <map>
#include <dirent.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <unistd.h>

#include "CSVReader.hpp"
#include "CSVIterator.hpp"
#include "CSVRowIterator.hpp"

using namespace std;

int main(int argc, char const *argv[])
{
    const string resultsPath = "labels/";
    vector<CSVIterator> labelIters;
    CSVReader c;

    DIR *dir = opendir(resultsPath.c_str());
    if (dir == NULL)
    {
        /* could not open directory */
        perror("opendir");
        return EXIT_FAILURE;
    }
    struct dirent *ent;
    while ((ent = readdir(dir)) != NULL)
    {
        string fileName = string(ent->d_name);
        if (fileName[0] != '.')
        {
            fileName = resultsPath + fileName;
            c.setFile(fileName);
            // c.printHead();
            labelIters.push_back(CSVIterator(c.getData()));
        }
    }
    closedir(dir);

    stringstream sout;
    sout << "Class Number" << endl;
    int ncol = labelIters.size();
    int nrow = c.getNumberOfRows() - 1;
    // cout << ncol << ' ' << nrow << endl;
    for (int i = 0; i < nrow; i++)
    {
        map<int, int> m;
        for (int j = 0; j < ncol; j++)
        {
            if (labelIters[j].hasRow())
            {
                CSVRowIterator it(labelIters[j].getNextRow());
                if (it.hasCell())
                {
                    CSVCell cell = it.getNextCell();
                    if (cell[0])
                    {
                        m[stoi(cell)]++;
                    }

                }
            }
        }
        int mx = 0, res = 0;
        for (auto it = m.begin(); it != m.end(); it++)
        {
            if (it->second > mx)
            {
                mx = it->second;
                res = it->first;
            }
        }
        sout << res << endl;
    }
    string result = sout.str();
    // cout << result;

    string fifo = "./res";
    int fd = open(fifo.c_str(), O_CREAT, S_IRWXU);
    if (fd < 0)
    {
        perror("open");
    }
    close(fd);

    if (mkfifo(fifo.c_str(), S_IRWXU | S_IRWXG | S_IRWXO) < 0)
    {
        // perror("mkfifo");
    }

    fd = open(fifo.c_str(), O_WRONLY | O_CREAT, S_IRWXU);
    if (fd < 0)
    {
        perror("open");
    }
    write(fd, result.c_str(), result.size() + 1);
    close(fd);
    return 0;
}
