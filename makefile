EnsembleClassifier_PROG = EnsembleClassifier.out
LinearClassifier_PROG = LinearClassifier.out
Voter_PROG = Voter.out

CFLAGS = -std=c++11 -Wall -pedantic -Iinclude
CC = g++ ${CFLAGS}

common_OBJS = $(patsubst common/%.cpp, obj/%.o, $(wildcard common/*.cpp))

LinearClassifier_OBJS = ${common_OBJS} $(patsubst LinearClassifier/%.cpp, obj/%.o, $(wildcard LinearClassifier/*.cpp))
Voter_OBJS = ${common_OBJS} $(patsubst Voter/%.cpp, obj/%.o, $(wildcard Voter/*.cpp))
EnsembleClassifier_OBJS = ${common_OBJS} $(patsubst EnsembleClassifier/%.cpp, obj/%.o, $(wildcard EnsembleClassifier/*.cpp))

.PHONY: all clean build run

all: clean build

run: build
	./${EnsembleClassifier_PROG} Assets/validation Assets/weight_vectors
	# diff res Assets/validation/labels.csv | grep '<' | wc -l

build: obj ${LinearClassifier_PROG} ${EnsembleClassifier_PROG} ${Voter_PROG}

${Voter_PROG}: ${Voter_OBJS} Voter.cpp
	$(CC) ${Voter_OBJS} Voter.cpp -o ${Voter_PROG}

${LinearClassifier_PROG}: ${LinearClassifier_OBJS} LinearClassifier.cpp
	$(CC) ${LinearClassifier_OBJS} LinearClassifier.cpp -o ${LinearClassifier_PROG}

${EnsembleClassifier_PROG}: ${EnsembleClassifier_OBJS} EnsembleClassifier.cpp
	$(CC) ${EnsembleClassifier_OBJS} EnsembleClassifier.cpp -o ${EnsembleClassifier_PROG}

obj/EnsembleClassifier.o: EnsembleClassifier/EnsembleClassifier.cpp include/EnsembleClassifier.hpp
	$(CC) -c EnsembleClassifier/EnsembleClassifier.cpp -o obj/EnsembleClassifier.o

obj/LinearClassifier.o: LinearClassifier/LinearClassifier.cpp include/LinearClassifier.hpp
	$(CC) -c LinearClassifier/LinearClassifier.cpp -o obj/LinearClassifier.o

obj/WeightVector.o: LinearClassifier/WeightVector.cpp include/WeightVector.hpp
	$(CC) -c LinearClassifier/WeightVector.cpp -o obj/WeightVector.o

obj/CSVReader.o: common/CSVReader.cpp include/CSVReader.hpp
	$(CC) -c common/CSVReader.cpp -o obj/CSVReader.o

obj/CSVIterator.o: common/CSVIterator.cpp include/CSVIterator.hpp
	$(CC) -c common/CSVIterator.cpp -o obj/CSVIterator.o

obj/CSVRowIterator.o: common/CSVRowIterator.cpp include/CSVRowIterator.hpp
	$(CC) -c common/CSVRowIterator.cpp -o obj/CSVRowIterator.o

obj:
	mkdir -p obj

clean:
	rm -r obj
	rm ${EnsembleClassifier_PROG}
	rm ${LinearClassifier_PROG}
	rm ${Voter_PROG}
