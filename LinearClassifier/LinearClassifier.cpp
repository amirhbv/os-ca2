#include "LinearClassifier.hpp"

#include "CSVIterator.hpp"
#include "CSVReader.hpp"

LinearClassifier::LinearClassifier(CSVData weightVectors)
{
    CSVIterator it(weightVectors);
    while(it.hasRow())
    {
        CSVRow weightVector = it.getNextRow();
        this->weights.push_back(WeightVector(weightVector));
    }
}

std::vector<ClassNumber> LinearClassifier::classify(std::string dataset)
{
    CSVReader c(dataset);
    return this->classify(c.getData());
}

std::vector<ClassNumber> LinearClassifier::classify(CSVData dataset)
{
    std::vector<ClassNumber> res;
    CSVIterator it(dataset);
    while (it.hasRow())
    {
        CSVRow dataRow = it.getNextRow();
        res.push_back(this->classifyRow(dataRow));
    }
    return res;
}

ClassNumber LinearClassifier::classifyRow(CSVRow dataRow)
{
    Score maxScore = 0, temp;
    ClassNumber res = 0;
    for (size_t i = 0; i < this->weights.size(); i++)
    {
        temp = this->weights[i].dotProduct(dataRow);
        if (temp > maxScore)
        {
            maxScore = temp;
            res = i;
        }
    }
    return res;
}
