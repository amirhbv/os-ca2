#include "WeightVector.hpp"

#include <string>
#include "CSVRowIterator.hpp"


WeightVector::WeightVector(CSVRow row)
{
    CSVRowIterator it(row);
    while (it.hasCell())
    {
        CSVCell cell = it.getNextCell();
        if (it.hasCell())
        {
            Weight val = stod(cell);
            this->weights.push_back(val);
        }
        else
        {
            Bias val = stod(cell);
            this->bias = val;
        }
    }
}

Score WeightVector::dotProduct(CSVRow dataRow)
{
    Score res = this->bias;
    CSVRowIterator it(dataRow);
    int i = 0;
    while (it.hasCell())
    {
        CSVCell cell = it.getNextCell();
        res += (stod(cell) * this->weights[i++]);
    }
    return res;
}
