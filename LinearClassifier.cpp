#include <iostream>
#include <sstream>
#include <fcntl.h>
#include <sys/stat.h>
#include <unistd.h>

#include "LinearClassifier.hpp"
#include "CSVReader.hpp"

using namespace std;

int main(int argc, char const *argv[])
{
    if (argc < 3)
    {
        cout << "Too few arguments." << endl;
        return EXIT_FAILURE;
    }

    const string weightVectors = string(argv[1]);
    CSVReader c1(weightVectors);
    // c1.printHead();

    LinearClassifier linearClassifier(c1.getData());

    const string dataset = string(argv[2]);
    vector<ClassNumber> res = linearClassifier.classify(dataset);

    stringstream sout;
    sout << "Class Number" << endl;
    for (auto &&i : res)
    {
        sout << i << endl;
    }
    string result = sout.str();

    int idx = weightVectors.find_last_of('_');
    mkdir("labels" , S_IRWXU | S_IRWXG | S_IRWXO);
    string fifo = "./labels/labels" + weightVectors.substr(idx);
    int fd = open(fifo.c_str(), O_CREAT, S_IRWXU);
    if (fd < 0)
    {
        perror("open");
    }
    close(fd);

    if (mkfifo(fifo.c_str(), S_IRWXU | S_IRWXG | S_IRWXO) < 0)
    {
        // perror("mkfifo");
    }

    fd = open(fifo.c_str(), O_WRONLY | O_CREAT, S_IRWXU);
    if (fd < 0)
    {
        perror("open");
    }
    write(fd, result.c_str(), result.size() + 1);
    close(fd);
    return 0;
}
