#include <iostream>
#include <fstream>
#include <vector>
#include <algorithm>
#include <dirent.h>
#include <cstdlib>
#include <unistd.h>
#include <sys/wait.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <iomanip>

#include "CSVReader.hpp"
#include "CSVIterator.hpp"

using namespace std;

#define READ_END 0
#define WRITE_END 1

void createVoter()
{
    pid_t pid = fork();
    if (pid == pid_t(0))
    {
        execl("./Voter.out", "Voter", (char *)NULL);

        perror("execl");
        exit(EXIT_FAILURE);
    }
    else if (pid > pid_t(0))
    {
        wait(NULL);
        // pid_t cpid = wait(NULL);
        // cout <<  cpid << endl;
    }
    else
    {
        perror("fork");
        exit(EXIT_FAILURE);
    }
}

void createLinearClassifier(string weightVectorsFile, string datasetPath)
{
    int p[2];
    if (pipe(p) == -1)
    {
        perror("pipe");
        exit(EXIT_FAILURE);
    }

    const char delima = '*';
    pid_t pid = fork();
    if (pid == pid_t(0))
    {
        const int MAX_BUFF_SIZE = 1024;
        char message[MAX_BUFF_SIZE + 1];
        int nbytes = read(p[READ_END], message, MAX_BUFF_SIZE);
        if (nbytes < 0)
        {
            perror("read");
            exit(EXIT_FAILURE);
        }
        message[nbytes] = '\0';

        string fileNames(message);
        int delimaIndex = fileNames.find(delima);
        string weightVectorsFileName = fileNames.substr(0, delimaIndex);
        string datasetFileName = fileNames.substr(delimaIndex + 1, fileNames.size());

        execl("./LinearClassifier.out", "LinearClassifier", weightVectorsFileName.c_str(), datasetFileName.c_str(), (char *)NULL);

        perror("execl");
        exit(EXIT_FAILURE);
    }
    else if (pid > pid_t(0))
    {
        string message = weightVectorsFile + delima + datasetPath;
        write(p[WRITE_END], message.c_str(), message.size());
        // pid_t cpid = wait(NULL);
        // cout <<  cpid << endl;
    }
    else
    {
        perror("fork");
        exit(EXIT_FAILURE);
    }
}

int main(int argc, char const *argv[])
{
    if (argc < 3)
    {
        cout << "Too few arguments." << endl;
        return EXIT_FAILURE;
    }

    const string validationPath = string(argv[1]);
    const string datasetPath = validationPath + "/dataset.csv";

    const string weightVectorsPath = string(argv[2]) + "/";
    vector<string> weightVectorsFiles;

    DIR *dir = opendir(weightVectorsPath.c_str());
    if (dir == NULL)
    {
        /* could not open directory */
        perror("opendir");
        return EXIT_FAILURE;
    }
    struct dirent *ent;
    while ((ent = readdir(dir)) != NULL)
    {
        string fileName = string(ent->d_name);
        if (fileName[0] != '.')
        {
            weightVectorsFiles.push_back(weightVectorsPath + fileName);
        }
    }
    closedir(dir);

    for (auto &&weightVectorsFile : weightVectorsFiles)
    {
        createLinearClassifier(weightVectorsFile, datasetPath);
    }

    // wait for all classifiers to finish
    while (wait(NULL) > 0)
        ;

    createVoter();

    const string labelsPath = validationPath + "/labels.csv";
    CSVIterator correctLabelsIter(CSVReader(labelsPath).getData());

    string fifo = "./res";
    if (mkfifo(fifo.c_str(), S_IRWXU | S_IRWXG | S_IRWXO) < 0)
    {
         // perror("mkfifo");
    }

    ifstream fin(fifo.c_str(), ifstream::in);
    string res;
    getline(fin, res);

    int all = 0, correct = 0;
    while (getline(fin, res))
    {
        if (correctLabelsIter.hasRow())
        {
            all++;
            correct += (res == correctLabelsIter.getNextRow()[0]);
        }
    }
    cout << fixed << setprecision(2);
    cout << "Accuracy: " << (double(correct * 100) / double(all)) << '%' << endl;

    return 0;
}
