#include "CSVIterator.hpp"

CSVIterator::CSVIterator(CSVData _data)
{
    this->data = _data;
    this->currentRow = 0;
}

bool CSVIterator::hasRow() const
{
    return this->currentRow < this->data.size();
}

CSVRow CSVIterator::getNextRow()
{
    return this->data[this->currentRow++];
}
