#include "CSVRowIterator.hpp"

CSVRowIterator::CSVRowIterator(CSVRow _row)
{
    this->row = _row;
    this->currentCell = 0;
}

bool CSVRowIterator::hasCell() const
{
    return this->currentCell < this->row.size();
}

CSVCell CSVRowIterator::getNextCell()
{
    return this->row[this->currentCell++];
}
