#include "CSVReader.hpp"

#include <iostream>
#include <fstream>
#include <sstream>

CSVReader::CSVReader()
{
    this->setFile("");
}

CSVReader::CSVReader(std::string fileName)
{
    this->setFile(fileName);
}

void CSVReader::setFile(std::string fileName)
{
    if (fileName != this->fileName)
    {
        this->fileName = fileName;
        this->header.clear();
        this->data.clear();
        this->isReady = false;
    }
}

void CSVReader::getReady()
{
    if (!this->isReady)
    {
        this->isReady = this->readCSV();
    }
}

CSVData CSVReader::getData()
{
    this->getReady();
    return data;
}

int CSVReader::getNumberOfRows()
{
    this->getReady();
    return data.size();
}

int CSVReader::getNumberOfColumns()
{
    this->getReady();
    return data[0].size();
}

bool CSVReader::readCSV()
{
    if (!this->fileName.size())
    {
        return false;
    }

    std::ifstream fin(this->fileName);
    std::string row;

    std::getline(fin, row);
    this->header = this->parseCSVRow(row);

    while (std::getline(fin, row))
    {
        this->data.push_back(this->parseCSVRow(row));
    }

    return true;
}

CSVRow CSVReader::parseCSVRow(std::string _row)
{
    std::stringstream rowStream(_row);
    CSVRow row;
    CSVCell cell;

    while (std::getline(rowStream, cell, CSV_DELIMITER))
    {
        row.push_back(cell);
    }

    return row;
}

void CSVReader::printHead()
{
    this->getReady();

    for (CSVCell cell : this->header)
    {
        std::cout << cell << ' ';
    }
    std::cout << std::endl;

    for (CSVCell cell : this->data.front())
    {
        std::cout << cell << ' ';
    }
    std::cout << std::endl;
}
