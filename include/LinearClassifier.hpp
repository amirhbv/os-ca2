#if !defined(LINEAR_CLASSIFIER_HPP)
#define LINEAR_CLASSIFIER_HPP

#include <vector>

#include "CSV.hpp"
#include "WeightVector.hpp"

typedef size_t ClassNumber;

class LinearClassifier
{
private:
    std::vector<WeightVector> weights;

    ClassNumber classifyRow(CSVRow);

public:
    LinearClassifier(CSVData);

    std::vector<ClassNumber> classify(CSVData);
    std::vector<ClassNumber> classify(std::string);
};

#endif // LINEAR_CLASSIFIER_HPP
