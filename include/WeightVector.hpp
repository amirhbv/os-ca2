#if !defined(WEIGHT_VECTOR_HPP)
#define WEIGHT_VECTOR_HPP

#include <vector>

#include "CSV.hpp"

typedef double Score;
typedef double Weight;
typedef double Bias;

class WeightVector
{
private:
    std::vector<Weight> weights;
    Bias bias;

public:
    WeightVector(CSVRow);
    Score dotProduct(CSVRow);
};

#endif // WEIGHT_VECTOR_HPP
