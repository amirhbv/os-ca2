#if !defined(CSV_READER_HPP)
#define CSV_READER_HPP

#include "CSV.hpp"

class CSVReader
{
private:
    bool isReady;
    std::string fileName;
    CSVHeader header;
    CSVData data;

    void getReady();
    bool readCSV();
    CSVRow parseCSVRow(std::string);

public:
    CSVReader();
    CSVReader(std::string);
    void setFile(std::string);

    CSVData getData();
    int getNumberOfRows();
    int getNumberOfColumns();

    void printHead();
};

#endif // CSV_READER_HPP
