#if !defined(CSV_ROW_ITERATOR_HPP)
#define CSV_ROW_ITERATOR_HPP

#include "CSV.hpp"

class CSVRowIterator
{
private:
    CSVRow row;
    size_t currentCell;

public:
    CSVRowIterator(CSVRow);

    bool hasCell() const;
    CSVCell getNextCell();
};

#endif // CSV_ROW_ITERATOR_HPP
