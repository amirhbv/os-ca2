#if !defined(CSV_ITERATOR_HPP)
#define CSV_ITERATOR_HPP

#include "CSV.hpp"

class CSVIterator
{
private:
    CSVData data;
    size_t currentRow;

public:
    CSVIterator(CSVData);

    bool hasRow() const;
    CSVRow getNextRow();
};

#endif // CSV_ITERATOR_HPP
