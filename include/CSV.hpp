#if !defined(CSV_HPP)
#define CSV_HPP

#include <string>
#include <vector>

const char CSV_DELIMITER = ',';

typedef std::string CSVCell;
typedef std::vector<CSVCell> CSVRow;
typedef CSVRow CSVHeader;
typedef std::vector<CSVRow> CSVData;

#endif // CSV_HPP
